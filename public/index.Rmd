---
pagetitle: "Clase 10: Intro Deep Learning"
title: "**Big Data and Machine Learning en el Mercado Inmobiliario**"
subtitle: "**Clase 10:** Intro Deep Learning"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [BD-ML](https://ignaciomsarmiento.github.io/teaching/BDML)
output: 
  html_document:
    theme: flatly
    highlight: haddock
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(knitr,tidyverse,fontawesome,kableExtra)
# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!---------------------->
## **[0.] Configuración inicial**

Para replicar las secciones de esta clase, primero debe descargar el siguiente [proyecto de R](https://gitlab.com/lectures-r/big-data-real-state-202202/clase-10/-/archive/main/clase-10-main.zip?path=clase-10) y abrir el archivo `clase-10.Rproj`. 

### **0.0 Instalar/llamar las librerías de la clase**

```{r , eval=T}
require(pacman)
p_load(tidyverse,rio,janitor,skimr,
       neuralnet) ## libreria para redes
```

<!---------------------->
## **[1.] Introduccion**

### **1.1 Elementos de una red neuronal** ###

```{r}
#### + Neuronas
# Los nodos de una red neuronal reciben el nombre de neurona
# Las neuronas se agrupan en capas

#### + Capas: una red neuronal tiene a lo menos 3 capas
# Input
# Output
# A lo menor una capa oculta

#### + Pesos/Coeficientes
# Son las ponderaciones que la red le asigna a cada neurona
# Resalta la importancia de la conexion en entre las neuronas

#### + Sesgo/Constante:
# Cada neurona tiene un sesgo (menor en la capa de input)

#### + Funcion de activacion:
# Define si una neurona se prende o no
# Las funciones de activacion agregan componentes no lineales a una red
# Funcion de escalon: {1 if x > 0 & 0 if x < 0} ; No suavisa el ajuste y en x == 0 no tiene derivada
# Funciones sigmoides (tienen forma de S) logistica: 1/(1 + e-x) ; desvanecimiento de gradiente y alto costo compuacional
# ReLU: Rectified Linear Unit - max{0,x} ; simple y bajo costo computacional, pero neuronas muertas
# Leaky ReLU: - max{0.01x,x} ; Evita las neuronas muertas pero desvanece el gradiente
# PReLU: Parametric ReLU - {x if x > 0 | ax if x <= 0}

#### + Optimizador: Algoritmo que le indica a la red como ajustar ajustar pesos y sesgos para que aprenda y no desaprenda
# tasa de aprendizaje (LR - learning rate): que tanto ajustar pesos y sesgos
# Con LR pequeña e aprendizaje es lento
# Con LR grande el aprendizaje es rapido pero se puede pasar del numero esperado
# Número de vueltas (veces en las que evalua los datos para hacer el ajuste)

#### + Funcion de perdida:
# Para medir que tan bien lo esta haciendo la red
# RMSE: Raiz del Error Cuadratico Medio 

### **1.2 Hiperparametos de una red neuronal** ###

# Numero de capas
# Numero de neuronas en la capa
# Otimizador
# Tasa de aprendizaje
# Numero de vueltas
# Funcion de activacion

### **1.3 Algoritmo de una red neuronal** ###

# 1. Tener el vector de inputs y el vector de outputs
# 2. Iniciar la red con valores aleatorios de pesos y sesgos
# 3. Hacer la prediccion (generar un output) para cada input
# 4. Comparar predicciones con valores observados 
# 5. Ajustar pesos y sesgos por backpropagation 
# 6. Repite pasos 3, 4 y 5
```

<!---------------------->
## **[2.] Red Neuronal Basica**

### **2.1 Modelo basico**

```{r}
## susesion de fibonachi: https://www.rpubs.com/MauricioSF/558913#:~:text=La%20secuencia%20de%20Fibonacci%20es,%2C%20233%2C%20377%2C%20%E2%80%A6.
fib <- function(n) {
       if (n == 1) {return(0)}
       else if(n == 2) {return(1)}
       else if(n > 2) {return(fib(n - 1) + fib(n - 2))}
}
fib(12)

## input
input <- 1:12

## output
output <- rep(NA,length(input))
output
for (i in 1:12){output[i] <- fib(i)}
output

## data
data <- tibble("y"=output,"x"=input) 
data

## fit model
nn_1 <- neuralnet(y ~ x,
                  data = data,
                  rep = 1, 
                  hidden = c(4), #  N neuronas por capa oculta 
                  threshold = 0.05,   # RL
                  algorithm = "rprop+", # "Resilient Backpropagation"
                  err.fct = "sse" # fucnion de perdida
)
```

```{r}
## plot model
plot(nn_1) ## azul=sesgo ; negro=pesos
```

```{r}
## funcion de activacion
nn_1$act.fct

## predicciones
data$y
nn_1$net.result

## plot observados vs predichos
data$y_nn1 <- nn_1$net.result[[1]]
ggplot(data=data , aes(x=y,y=y_nn1)) + 
geom_point(col="blue") + theme_test()

## rmse
with(data = data , sqrt(mean( (y-y_nn1)^2 )))

## computar predicciones
compute(nn_1,tibble("x"=13))
fib(13)

### **2.2 Aumentar el numero de repeticiones**

## fit model
nn_2 <- neuralnet(y ~ x,
                  data = data,
                  rep = 10, 
                  hidden = c(4), #  N neuronas por capa oculta 
                  threshold = 0.05,   # RL
                  algorithm = "rprop+", # "Resilient Backpropagation"
                  err.fct = "sse" # fucnion de perdida
)

## variacion en el erro
nn_2$result.matrix
```

```{r}
## plot
error <- data.frame("sse"=nn_2$result.matrix[1,] , n=1:10)
ggplot(data=error , aes(x=n , y=sse)) + 
geom_line(col="red") + geom_point(col="black") + theme_test()
```

## **[3.] Aplicacion: Precio de las viviendas**

```{r}
### **3.1 prepare db**
rm(list=ls())

## load data
db <- import("input/data_regresiones.rds")

## tibble to matrix
df <- model.matrix(~.+I(bathrooms^2)+I(surface_total^2)+I(dist_cbd^2)-1-price , data = db)

## reescalar las variables
df <- scale(x = df , center = T , scale = T)

## limpiar los nombres de las variables
df <- as_tibble(df) %>% clean_names()
df$price <- db$price/1000000

## particionar la base de datos
set.seed(2104)
sample <- sample(x = nrow(df) , size = 0.2*nrow(df))
test <- df[sample,]
train <- df[-sample,]

### **3.2 Fit model**
model_1 <- neuralnet(price ~.,
                     data = train[1:100,],
                     rep = 5, 
                     hidden = c(2,2),
                     threshold = 0.05)

## **3.3 Computar el RMSE**
plot(model_1)
```









